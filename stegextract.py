import argparse
import subprocess

parser = argparse.ArgumentParser(description="Use dictionary to crack password of steghide embedded file")

required = parser.add_argument_group('Required arguments')

required.add_argument('-sf', help="The stego file with the embedded data", required=True)
required.add_argument('-d', help="The wordlist containing the password", required=True)
parser.add_argument('-o', help="The output file", required=False)

args=parser.parse_args()

if(args.o != None) :
	try:
		with open(args.d) as file:
			data = file.readlines()
			for line in data:
				line = line.replace('\n', '')
				try:

					output = subprocess.call(["steghide", "extract", "-sf", args.sf, "-p", line, '-xf', args.o])
					print("The password = " + line)
					break
				except subprocess.CalledProcessError:
					print("Trying " + line)


	except FileNotFoundError:
		print("Dictionary file not found")

else :
	try:
		with open(args.d) as file:
			data = file.readlines()
			for line in data:
				line = line.replace('\n', '')
				try:

					output = subprocess.call(["steghide", "extract", "-sf", args.sf, "-p", line])
					print("The password = " + line)
					break
				except subprocess.CalledProcessError:
					print("Trying " + line)


	except FileNotFoundError:
		print("Dictionary file not found")
